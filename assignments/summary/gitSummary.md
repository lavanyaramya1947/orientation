# **Summary**
## **Git**

Git is a version control system.It is used to locally track changes in your project or folder and push and pull changes from repositries like **Gitlab**.Git lets you easily keep track of every revision you and your team make during the development of your software.
Basically, if you’re not using git, you’re coding with one hand tied behind your back.

![see this](https://git-scm.com/book/en/v2/images/deltas.png)


## **Git workflow**

 ![Visit here](https://images.osteele.com/2008/git-workflow.png)



## **Required Vocabulary**


**Repository**

 A place,room or container where something is deposited or stored.It is also called **repo**. Generically refers to a central place where data is stored and maintained. A repository can be a place where multiple databases or files are located for distribution over a network, or a repository can be a location that is directly accessible to the user without having to travel across a network.

**Commit**

The **commit** command is used to save your changes to the local repository. Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment.

**Push**

**Push**ing is essentially syncing your commits to GitLab.

**Branch**

A **branch** in Git is simply a lightweight movable pointer to one of these commits. The default branch name in Git is master. As you start making commits, you're given a master branch that points to the last commit you made.

**Merge**

Merging is Git's way of putting a forked history back together again. The git merge command lets you take the independent lines of development created by git branch and integrate them into a single branch.

**Clone**

It takes the entire online repository and makes an exact copy of it on your local machine.

**Fork**

**Fork**ing is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

## **Git Installation**

Git (probably) didn’t come installed on your computer, so we have to get it there. Luckily, installing git is super easy, whether you’re on Linux, Mac, or Windows.

For Linux, open the terminal and type  - sudo apt-get install git 

On Windows, it’s just as simple. You download the installer and run it.

**To check version**

To check your version just type this command - git --version

## **Git Internals**

Git has three main states that your files can reside in:

**1.Modified** means that you have changed the file but have not committed it to your
repo yet.


**2.Staged** means that you have marked a modified file in its current version to go into
your next picture/snapshot.


**3.Committed** means that the data is safely stored in your local repo in form of
pictures/snapshots.



## **Basic Commands**

1. _git clone_ - To clone the repo.
2. _git commit_ - To commit to the repo.
3. _git push_ - To push to the local repository.
4. _git checkout_  - To create a branch.
5. _git diff_  -  It is used to view the conflicts between branches before merging them.
6. _git show_  - View information about any git object.
7. _git pull_ - Git pull merges all the changes present in the remote repository to the local working directory.
8. _git merge_ - Merge a branch into the active one.
9. _git status_ -This command returns the current state of the repository.
git status will return the current working branch. If a file is in the staging area, but not committed, it shows with git status. Or, if there are no changes it’ll return nothing to commit, working directory clean.
 10. _git add ._ -to add untracked files.