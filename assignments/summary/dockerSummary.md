# **Summary**

## **Docker**

Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. 

![Docker](https://raw.githubusercontent.com/docker-library/docs/c350af05d3fac7b5c3f6327ac82fe4d990d8729c/docker/logo.png)
## **Docker Terminologies**

**Docker**

Docker is a program for developers to develop, and run applications with containers.

**Container**

An instance of a Docker image. A container represents the execution of a single application, process, or service. It consists of the contents of a Docker image, an execution environment, and a standard set of instructions.


**Container image**


 A package with all the dependencies and information needed to create a container. An image includes all the dependencies (such as frameworks) plus deployment and execution configuration to be used by a container runtime.

 **Tag**


A mark or label you can apply to images so that different images or versions of the same image (depending on the version number or the target environment) can be identified.

## **Illustration**

![Work Flow](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/10/Docker-Architecture-What-Is-Docker-Container-Edureka.png)
## **Docker Installation**

All in one

Windows ,Linux and Mac  you can find here [Click here](https://docs.docker.com/get-docker/)

## **Basic Commands**

**docker ps** : Allows us to view all the containers that are running on the Docker Host.

**docker start** : Starts any stopped container(s).

**docker stop** : Stops any running container(s).

**docker run** : reates containers from docker images.

**docker rm** : Deletes the container.


## **Common Operations**

   1. docker run – Runs a command in a new container.
   2. docker start – Starts one or more stopped containers
   3.  docker stop – Stops one or more running containers
   4. docker build – Builds an image form a Docker file
   5. docker pull – Pulls an image or a repository from a registry
   6. docker push – Pushes an image or a repository to a registry
   7.  docker search – Searches the Docker Hub for images
   8.  docker attach – Attaches to a running container
   9.  docker commit – Creates a new image from a container’s changes

## Examples
1. Find the docker version.
```bash
docker –version
```
2. To pull images from the docker repository.
```bash
docker pull <image name>
```
3.This command is used to create a container from an image
```bash
Usage: docker run -it -d <image name>
```

4.This command is used to list the running containers
```bash
docker ps
```

5. This command is used to access the running container

```bash
docker exec -it <container id> bash
```
6.This command kills the container by stopping its execution immediately.
```bash
docker kill <container id>
```
7. This command creates a new image of an edited container on the local system
```bash
docker commit <conatainer id> <username/imagename>
```

 8. Push docker image to the dockerhub.

Tag image name with a different name

```bash
docker tag <image-author/image-name>/trydock <username>/<name-of-Image>
```
 9. Push to dockerhub

```bash
docker push <username>/<name-of-Image>
```




